import React from 'react';
import {
    Animated, 
    Easing,
    interpolate
} from "react-native-reanimated";




 export const mix = (animatedValue, start, end) => {
    return interpolate([0, 1], [start, end])
}