/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 /**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

  import React from 'react';

  import { NavigationContainer } from "@react-navigation/native";
  
  import { createStackNavigator } from "@react-navigation/stack";
  import { navigationRef } from './utils/navigation';
  

  import Home from './screens/Home';
  import MapPage from './screens/Map';
  import MenuBall from './screens/MenuBall';
  
  
  
  const Stack = createStackNavigator();
  
  const screenOptionStyle = {
    headerStyle: {
      backgroundColor: "white",
    },
    headerTintColor: "#3681cc",
    headerBackTitle: "Back",
  };
  
  
  const App = () => {
    return (
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator initialRouteName="Home" screenOptions={screenOptionStyle}>
          <Stack.Screen 
                name="Home" 
                component={Home} 
                options={{
                  headerShown: false
                }}
            />
          <Stack.Screen 
            name="MenuBall" 
            component={MenuBall} 
            options={{
              headerShown: true
            }}
          />
          <Stack.Screen 
            name="Map" 
            component={MapPage} 
            options={{
              headerShown: true
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  };
  
  export default App;
  