import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Alert } from 'react-native';




const Home = (props) => {
    const { navigation: { navigate } } = props;
    return(
        <View style={styles.container}>
            <TouchableOpacity onPress={() => navigate('MenuBall')} style={styles.wrapper}>
                    <Text style={styles.menu}>MenuBall</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('Map')} style={styles.wrapper}>
                    <Text style={styles.menu}>Map</Text>
            </TouchableOpacity>
        </View>
    )
}


export default Home;



const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        paddingTop: 50
    },
    menu: {
        fontSize: 20,
        color: '#3681cc',
        fontWeight: 'bold'
    },
    wrapper: {
        padding: 20
    }
});