import React, { useRef, useEffect } from "react";
import { Animated, View, StyleSheet, PanResponder, Text, Dimensions } from "react-native";

const Ball = () => {
  const pan = useRef(new Animated.ValueXY()).current;

  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderGrant: () => {
        pan.setOffset({
          x: pan.x._value,
          y: pan.y._value
        });
      },
      onPanResponderMove: Animated.event(
        [
          [],
          
          { 
              dx: pan.x, 
              dy: pan.y,
            },
        ]),
      onPanResponderRelease: (e, gestureState) => {
        pan.flattenOffset();
        _setPosition(gestureState)
        Animated.spring(
          pan,
          {toValue: {x: 0, y: 0}}
        ).start()
      }
    })
  ).current;

  const _setPosition = (gesture) => {
    const dx = gesture.moveX - gesture.x0
    const dy = gesture.moveY - gesture.y0
    const Window = Dimensions.get('window');
    const { width, height } = Window;
    //const x = Math.abs(this.offset.fx + dx)
    //const y = Math.abs(this.offset.fy + dy)

    //const idTo = (Math.floor(x / width) + Math.floor(y / height) * COLUMNS)
    
    //this.props.setPosition(gesture, this.props.idx, idTo)
  }


  useEffect(() => {
      console.log(pan.x);
  },[pan]);

  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          transform: [{ translateX: pan.x }, { translateY: pan.y }]
        }}
        {...panResponder.panHandlers}
        //style={pan.getLayout()}
      >
        <View style={styles.box} pointerEvents="none"/>
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  titleText: {
    fontSize: 14,
    lineHeight: 24,
    fontWeight: "bold"
  },
  box: {
    height: 50,
    width: 50,
    backgroundColor: "blue",
    borderRadius: 25
  }
});

export default Ball;