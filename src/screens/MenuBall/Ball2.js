import React, { Component, useMemo,useEffect } from 'react'
import {
  StyleSheet, Text,TouchableOpacity, Image
  } from 'react-native';
  import Animated, {
    useAnimatedStyle, 
    useSharedValue,
    withSpring,
    useAnimatedGestureHandler,
    withTiming,
    withSequence,
    Easing,
    onGestureEvent,
    useDerivedValue,
    
} from 'react-native-reanimated';
import {Context} from './index';

import {PanGestureHandler} from 'react-native-gesture-handler';


const Ball2 = (props) => {

    const {
        setOffsetForAll,
        currentBallPos,
        name,
        renderText,
        imageSource,
        offSet,
        index,
        X,
        Y,
        currentBall
    } = props;

    const sizeXY = useSharedValue(1);

    const isShowingText = useSharedValue(0);
    const pressed = useSharedValue(false);
    const startingPosition = 0;
    const translateX = useSharedValue(X);
    const translateY = useSharedValue(Y);
    //const MenuProps = React.useContext(Context);
    const currentMenuIsInRange = useSharedValue(false);



    
    const reduceBallSize = () => {
        'worklet';
        isShowingText.value = 0;
         sizeXY.value = 1;
      };


    useEffect(() => {
        setLocationHook();
        findMenuLocation();
        //console.log(offSet);
        //console.log(currentBall)

    },[offSet,currentMenuIsInRange]);

    const handleShortPressOne = () => {
        'worklet';
        sizeXY.value = 3; 
        isShowingText.value = 1;
        translateX.value = 0;
        translateY.value = 0;
    };
    
  

    const handleOnLayout = React.useCallback((event) => {
        const {height, width} = event.nativeEvent.layout;
        const {x, y} = event.nativeEvent.layout;
        setOffsetForAll([
            ...offSet,
            {'name': name, x: X, y: Y}
            //{[name] : { x: x, y: y}}
        ]);
    }, [offSet]);

    const getRandomInt = (max) => {
        return Math.floor(Math.random() * max);
    }
      

    const isBigEnough = (value) => {
        checkerx1 = value.x >= (translateX.value - 2);
        checkerx2 = value.x <= (translateX.value + 2);
        checkery1 = value.y >= (translateY.value - 2);
        checkery2 = value.y <= (translateY.value + 2);

        if(value.name !== name && checkerx1 && checkerx2 && checkery2 && checkery2){
            currentMenuIsInRange.value = value.name === name ? true : false;
           return value;
        }
    }

    const findMenuLocation = React.useCallback(() => { 
        if(offSet.length > 0){
            const result = offSet.filter(isBigEnough);
            if(result.length > 0 && currentMenuIsInRange.value === true){
                console.log(result)
                translateX.value = withSpring(translateX.value - getRandomInt(50));
                translateY.value = withSpring(translateY.value + getRandomInt(50));
            }
        }
    },[offSet]);

    const setLocationHook = React.useCallback(() => {
        'worklet';
        //offSet[index] = {[name]: {x: translateX.value, y: translateY.value}}
        offSet[index] = {'name': name, x: translateX.value, y: translateY.value}
        setOffsetForAll([...offSet]);
    }, [offSet]);
    
    const uas = useAnimatedStyle(() => {
        return {
          backgroundColor: pressed.value ? '#FEEF86' : '#001972',
          transform: [
              { scale: withSpring(sizeXY.value, {
                duration: 500,
                easing: Easing.bezier(0.25, 0.1, 0.25, 1),
            })},
              { translateX: translateX.value }, 
              { translateY: translateY.value },
            ],
        };
    });

    const text = useAnimatedStyle(() => {
        return {
            opacity: withSpring(isShowingText.value)          
        };
    });

    const eventHandler = useAnimatedGestureHandler({
        onStart: (event, ctx) => {
          pressed.value = true;
          //ctx.startY = translateY.value;
          //ctx.startX = translateX.value;
        },
        onActive: (event, ctx) => {
            translateX.value = event.translationX;
            translateY.value = event.translationY;
        },
        onEnd: (event, ctx) => {
            pressed.value = false;
            sizeXY.value = 1;
            
                    //x.value = withSpring(startingPosition);
          //y.value = withSpring(startingPosition);
        },
      });
    return (
        <PanGestureHandler onGestureEvent={eventHandler}>
            <Animated.View style={[styles.ball, uas]}>
                <TouchableOpacity
                    style={[styles.ball]} 
                    onPress={() => handleShortPressOne()}
                    onLongPress={() => reduceBallSize()}
                    onLayout={handleOnLayout}
                >
                    {imageSource && <Image source={imageSource} style={styles.image}/>}
                    <Animated.Text style={[styles.text, text]}>{renderText}</Animated.Text>
                </TouchableOpacity>
            </Animated.View>
        </PanGestureHandler>

    );
}  


export default Ball2

const styles = StyleSheet.create({
    text: {
        color: '#fff', 
        textAlign: 'center', 
        position: 'absolute', 
        alignSelf: 'center', 
        position: 'absolute'},
    ball: {width: 100, height: 100, borderRadius: 50, 
        backgroundColor: '#001972',
        position: 'relative',
        alignItems: 'center',
        justifyContent: "center", 

},
    image: {position: 'relative', alignSelf: 'center',width: 25, height: 25, opacity: 0.4}
  });
  