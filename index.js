/**
 * @format
 */

import {AppRegistry} from 'react-native';
import AppleMenu from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => AppleMenu);
